### Yolo-for-k210模型训练

+ 开发环境（需要安装的部分重要模块）
> + `tensorflow-gpu==1.15.0` 
> + `h5py==2.10` 
> + `imgaug` 
> + `scikit-image`
> + `tensorflow-model-optimization==0.1.1`
+ 数据集准备
> 通过vott工具对数据集进行标注，导出为PascalVoc格式。将导出后的文件夹复制到Train_Image下，并修改一级目录名称为MyImage
+ 在Train_Image下，按照7:2:1的比例，生成训练集、验证集、测试集文件
```bash
# 仅根据Annotations和JPEGImages目录 生成pscalvoc.txt、train.txt、val.txt、test.txt。同时会删除多余或不配对的 .xml .jpg 运行时间较长
python datamaking.py 
```
```bash
# 仅根据Annotations目录下文件的.xml文件生成需要的pscalvoc.txt、train.txt、val.txt、test.txt。运行时间较短，适用于确认数据集一一对应的情况
python datamakingv2.py
```
+ 退回到根目录，使用voc_label.py生成标签数据
```bash
python voc_label.py
cat MyImage_train.txt MyImage_val.txt> train.txt  # Linux使用此命令
type MyImage_train.txt MyImage_val.txt> train.txt # windowns使用此命令
```
> 注意： 改变路径后重新训练需从`python voc_label.py`重新开始
+ 将数据集路径和生成的标签数据合并到一个 .npy 文件
```bash
python make_voc_list.py train.txt data/voc_img_ann.npy
```
+ 加载注释，生成anchors
```bash
make anchors DATASET=voc ANCNUM=3 LOW="0.0 0.0" HIGH="1.0 1.0"
```
> 注意：结果是随机的，如果出现了类似于NaN的报错信息，就重新运行该命令，直到成功生成两组anchors数组为止

> 如果要使用自定义数据集，只需编写脚本并生成`data/{dataset_name}_img_ann.npy`，然后使用`make anchors DATASET=dataset_name`
+ 下载预训练模型到data文件夹中

| `MODEL`       | `DEPTHMUL` | Url                                                                                | Url                                        |
| ------------- | ---------- | ---------------------------------------------------------------------------------- | ------------------------------------------ |
| yolo_mobilev1 | 0.5        | [google drive](https://drive.google.com/open?id=1SmuqIU1uCLRgaePve9HgCj-SvXJB7U-I) | [weiyun](https://share.weiyun.com/59nnvtW) |
| yolo_mobilev1 | 0.75       | [google drive](https://drive.google.com/open?id=1BlH6va_plAEUnWBER6vij_Q_Gp8TFFaP) | [weiyun](https://share.weiyun.com/5FgNE0b) |
| yolo_mobilev1 | 1.0        | [google drive](https://drive.google.com/open?id=1vIuylSVshJ47aJV3gmoYyqxQ5Rz9FAkA) | [weiyun](https://share.weiyun.com/516LqR7) |
| yolo_mobilev2 | 0.5        | [google drive](https://drive.google.com/open?id=1qjpexl4dZLMtd0dX3QtoIHxXtidj993N) | [weiyun](https://share.weiyun.com/5BwaRTu) |
| yolo_mobilev2 | 0.75       | [google drive](https://drive.google.com/open?id=1qSM5iQDicscSg0MYfZfiIEFGkc3Xtlt1) | [weiyun](https://share.weiyun.com/5RRMwob) |
| yolo_mobilev2 | 1.0        | [google drive](https://drive.google.com/open?id=1Qms1BMVtT8DcXvBUFBTgTBtVxQc9r4BQ) | [weiyun](https://share.weiyun.com/5dUelqn) |
| tiny_yolo     |            | [google drive](https://drive.google.com/open?id=1M1ZUAFJ93WzDaHOtaa8MX015HdoE85LM) | [weiyun](https://share.weiyun.com/5413QWx) |
| yolo          |            | [google drive](https://drive.google.com/open?id=17eGV6DCaFQhVoxOuTUiwi7-v22DAwbXf) | [weiyun](https://share.weiyun.com/55g6zHl) |

> 注：mobilev 不是原创的，原作者有修改它适合 K210
+ 开始训练。使用MobileNet时，需要指定DEPTHMUL参数
```bash
# 普通训练命令
make train MODEL=yolo_mobilev1 DEPTHMUL=0.75 MAXEP=10 ILR=0.001 DATASET=voc CLSNUM=20 IAA=False BATCH=8

# 使用CKPT指令继续训练
make train MODEL=yolo_mobilev1 DEPTHMUL=0.75 MAXEP=10 ILR=0.001 DATASET=voc CLSNUM=20 IAA=False BATCH=8 CKPT=log/xxxxxxxxx/yolo_model.h5

# IAA增强数据
make train MODEL=yolo_mobilev1 DEPTHMUL=0.75 MAXEP=10 ILR=0.001 DATASET=voc CLSNUM=20 IAA=True BATCH=8 CKPT=log/xxxxxxxxx/yolo_model.h5
```
+ 对生成的模型进行验证
```
make inference MODEL=yolo_mobilev1 DEPTHMUL=0.75 CLSNUM=20 CKPT=log/xxxxxx/yolo_model.h5 IMG=data/xxx.jpg
```
+ 将模型由.h5转换为.tflite
```bash
toco --output_file mobile_yolo.tflite --keras_model_file log/xxxxxx/yolo_model.h5
```
+ 使用ncc量化工具将模型量化为kmodel
```bash
ncc mobile_yolo.tflite mobile_yolo.kmodel -i tflite -o k210model --dataset {images_dir}
```


### 参考资料

+ kendryte 官方仓库
    + nncase 量化工具：https://github.com/kendryte/nncase/releases/tag/v0.1.0-rc5 / https://github.com/kendryte/nncase/releases/tag/v0.2.0-alpha1
    + sdk：https://github.com/kendryte/kendryte-standalone-sdk
    + toolchain：https://github.com/kendryte/kendryte-gnu-toolchain/releases/tag/v8.2.0-20190409
+ [zhen8838/K210_Yolo_framework](https://github.com/zhen8838/K210_Yolo_framework)
+ [Yahboom 开发环境搭建教程](https://www.yahboom.com/build.html?id=3824&cid=406)
+ SEASKY yolo-for-k210 项目：
    > + 这个项目提供了一个在windows平台下进行模型训练并且把模型部署到**k210**上的完整项目
    > + 包含了两个子项目：一是模型训练的部分，目标检测模型使用的是**yolov3**；二是基于**kendyrate**官方用c++写的sdk构建的，一个能够加载**kmodel**并调用开发板摄像头传输过来的图片进行预测的完整项目
    > + 在训练时，需要注意python相关模块的版本；在部署时，需要注意修改main.c中的部分参数，包括自定义数据集类别数、anchor值等等。
    + 仓库地址：https://github.com/SEASKY-Master/SEASKY_K210 / https://github.com/SEASKY-Master/Yolo-for-k210
    + 视频讲解：https://www.bilibili.com/video/BV1rK4y1s7zh
+ 数据集链接
    > + 数据集链接:https://pan.baidu.com/s/1PCX5y3DrOvAQVTWtoYXhnA
    > + 提取码:e1cp
